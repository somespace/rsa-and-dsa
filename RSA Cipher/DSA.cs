﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace RSA_Cipher
{
    static class DSA
    {
        public static string MessagePath { get; private set; } = null;

        public static void SetMessagePath(string messagePath)
            => MessagePath = messagePath;


        public static string GetMessageFromFile(string path)
        {
            return File.ReadAllText(path);
        }


        public static bool DataToFile(string path, string data)
        {
            try
            {
                File.WriteAllText(path, data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string[] ReadKeysFromFile(string path)
        {
            try
            {
                string[] keyPair = File.ReadAllText(path).Split(' ');
                if (keyPair.Length != 2)
                    return null;
                return keyPair;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Chyba {e.Message}",
                    "Chyba čítania kľúča",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
        }

        public static bool KeysToFile(string path, string key1, string key2)
        {
            string keyPair = key1 + " " + key2;
            return DataToFile(path, keyPair);
        }

        private static string GetHashFromString(string text)
        {
            string hashFromString = "";

            SHA256Managed hasher = new SHA256Managed();
            byte[] hashedBytes = hasher.
                ComputeHash(Encoding.UTF8.GetBytes(text));

            hashFromString = BitConverter.ToString(hashedBytes).
                        Replace("-", "").ToLower();

            return hashFromString;
        }

        private static string GetHashFromFile()
        {
            string hashFromFile = "";
            try
            {
                CultureInfo.CurrentCulture = new CultureInfo("cz-cs");
                using (FileStream fileStream = File.OpenRead(MessagePath))
                {
                    SHA256Managed hasher = new SHA256Managed();
                    byte[] hashedBytes = hasher.ComputeHash(fileStream);

                    hashFromFile = BitConverter.ToString(hashedBytes).
                        Replace("-", "").ToLower();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show($"Chyba {e.Message}",
                    "Chyba súboru",
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
            return hashFromFile;
        }

        public static string GenerateFileSignature()
        {
            string digest = "";
            string hashFromFile = GetHashFromFile();

            if (hashFromFile == string.Empty)
            {
                return hashFromFile;
            }
            else
            {
                Cipher.EncryptMessage(hashFromFile);
                digest = string.Join(MainForm.separator, Cipher.EncryptedMessage);
            }
            
            return digest;
        }

        public static bool SignatureVerified(string message)
        {
            // hash originalnej spravy
            string originalMessageHash = GetHashFromString(message);

            // desifrovany hash
            string signatureHash = Cipher.DecryptMessage().Trim();

            if (originalMessageHash == signatureHash)
                return true;

            return false;
        }
    }
}
