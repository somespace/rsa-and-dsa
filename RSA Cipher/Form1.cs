﻿using System;
using System.IO;
using System.IO.Compression;
using System.Numerics;
using System.Windows.Forms;

namespace RSA_Cipher
{
    public partial class MainForm : Form
    {
        string pubMod = "";
        string priMod = "";
        string pubExp = "";
        string priExp = "";

        public const string separator = "\n\n";
        private string keysPath = "";
        public MainForm()
        {
            InitializeComponent();
            MainTabControl.TabPages.Remove(EncryptionTabPage);
            MainTabControl.TabPages.Remove(DecryptionTabPage);
            MainTabControl.TabPages.Remove(SignatureTabPage);
            ExponentCBox.SelectedItem = "Premenlivý";
            BitLengthCBox.SelectedItem = "2048";
        }

        public static BigInteger[] TextToNumBlocks(string text)
        {
            string textSplitByChar = text.Replace(separator, " ").Trim();
            string[] numbersAsText = textSplitByChar.Split(' ');

            BigInteger[] numberBlocks = new BigInteger[numbersAsText.Length];

            for (int i = 0; i < numbersAsText.Length; i++)
            {
                numberBlocks[i] = BigInteger.Parse(numbersAsText[i]);
            }

            return numberBlocks;
        }

        private void ShowTabs()
        {
            if (MainTabControl.TabCount == 1)
            {
                MainTabControl.TabPages.Add(EncryptionTabPage);
                MainTabControl.TabPages.Add(DecryptionTabPage);
                MainTabControl.TabPages.Add(SignatureTabPage);
            }
        }

        private void GenerateKeysButton_Click(object sender, EventArgs e)
        {
            Cipher.SetKeyBitSize(Convert.ToInt32(BitLengthCBox.SelectedItem.ToString()));
            
            string exponentSize = ExponentCBox.SelectedItem.ToString();
            bool isFixedKeySelected = exponentSize.Contains("65537") ? true : false;

            Cipher.GenerateKeys(isFixedKeySelected);

            PublicModulRTBox.Text = Cipher.n.ToString();
            PrivateModulRTBox.Text = Cipher.n.ToString();
            PublicExponentRTBox.Text = Cipher.e.ToString();
            PrivateExponentRTBox.Text = Cipher.GetPrivateExponent().ToString();

            if (MainTabControl.TabCount == 1)
            {
                ShowTabs();
            }

            MessageBox.Show("Kľúčový pár bol vygenerovaný.", "Hotovo",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void EncryptButton_Click_1(object sender, EventArgs e)
        {
            string message = OriginalTextRTBox.Text;
            if (message.Length == 0)
            {
                MessageBox.Show("Zadajte prosím správu k zašifrovaniu.",
                    "Prázdna správa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Cipher.EncryptMessage(message);
                EncryptedTextRTBox.Text = string.Join(separator, Cipher.EncryptedMessage);
            }
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            EncryptedTextRTBox.Focus();
            EncryptedTextRTBox.SelectAll();
            EncryptedTextRTBox.Copy();
        }

        private void InsertButton_Click(object sender, EventArgs e)
        {
            TextToDecryptRTBox.Clear();
            TextToDecryptRTBox.Paste();
        }

        private void DecryptButton_Click(object sender, EventArgs e)
        {
            string encryptedText = TextToDecryptRTBox.Text;

            if (encryptedText.Length == 0)
            {
                MessageBox.Show("Zadajte prosím správu k dešifrovaniu.",
                    "Prázdna správa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Cipher.SetEncryptedMessage(TextToNumBlocks(encryptedText));
                DecryptedTextRTBox.Text = Cipher.DecryptMessage().Trim();
            }
        }

        private void ClearEncryptTabButton_Click(object sender, EventArgs e)
        {
            OriginalTextRTBox.Text = "";
            EncryptedTextRTBox.Text = "";
        }

        private void ClearDecryptTabButton_Click(object sender, EventArgs e)
        {
            TextToDecryptRTBox.Text = "";
            DecryptedTextRTBox.Text = "";
        }

        // DSA 
        private void InsertKeysButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Vyberte priečinok kľúčov";
            
            if (keysPath.Length != 0)
                fbd.SelectedPath = keysPath;

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                string selectedDir = fbd.SelectedPath.ToString();
                string[] pubKeyFiles = Directory.GetFiles(selectedDir, "*.pub");
                string[] priKeyFiles = Directory.GetFiles(selectedDir, "*.pri");

                if (pubKeyFiles.Length != 0 && priKeyFiles.Length != 0)
                {
                    string firstPubKeyFile = pubKeyFiles[0];
                    string firstPriKeyFile = priKeyFiles[0];

                    string[] pubKeyPair = DSA.ReadKeysFromFile(firstPubKeyFile);
                    string[] priKeyPair = DSA.ReadKeysFromFile(firstPriKeyFile);

                    if ( pubKeyPair != null || priKeyPair != null)
                    {
                        pubMod = pubKeyPair[0];
                        pubExp = pubKeyPair[1];
                        priMod = priKeyPair[0];
                        priExp = priKeyPair[1];

                        PublicModulRTBox.Text = pubMod;
                        PublicExponentRTBox.Text = pubExp;
                        PrivateModulRTBox.Text = priMod;
                        PrivateExponentRTBox.Text = priExp;

                        Cipher.n = Cipher.TextToBigInt(pubMod);
                        Cipher.e = Cipher.TextToBigInt(pubExp);
                        Cipher.SetPrivateExponent(Cipher.TextToBigInt(priExp));

                        ShowTabs();

                        MessageBox.Show("Kľúče boli úspešne vložené.", 
                            "Kľúče vložené",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Niektorý z kľúčových súborov chýba, skúste zvoliť iný priečinok.",
                    "Chýba kľúč", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private bool KeysPresent()
        {
            pubMod = PublicModulRTBox.Text;
            priMod = PrivateModulRTBox.Text;
            pubExp = PublicExponentRTBox.Text;
            priExp = PrivateExponentRTBox.Text;

            if (pubMod == "" || priMod == "" || pubExp == "" || priExp == "")
            {
                MessageBox.Show("Niektorý z kľúčov chýba, vložte prosím kľúče.",
                    "Chýbajúce údaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool KeysWereSaved(string keysPath)
        {
            string pubKeyPath = Path.Combine(keysPath, "verejny_kluc.pub");
            string priKeyPath = Path.Combine(keysPath, "privatny_kluc.pri");

            bool pubKeySaved = DSA.KeysToFile(pubKeyPath, pubMod, pubExp);
            bool priKeySaved = DSA.KeysToFile(priKeyPath, priMod, priExp);
            
            if (pubKeySaved && priKeySaved)
                return true;
            return false;
        }

        private void ExportKeysButton_Click(object sender, EventArgs e)
        {
            if (!KeysPresent())
                return;

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Vyberte priečinok kľúčov";

            if (fbd.ShowDialog() == DialogResult.OK)
            {
                keysPath = fbd.SelectedPath;

                if (KeysWereSaved(keysPath))
                {
                    MessageBox.Show(
                        "Kľúče boli úspešne uložené na adrese:\n\n " +
                        keysPath, "Úspešne uložené", 
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(
                        "Kľúče možno existujú, skúste zvoliť iný priečinok.",
                        "Neznáma chyba",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
            }
        }

        private void SaveMessageButton_Click_1(object sender, EventArgs e)
        {
            string message = MessageRTBox.Text;

            if (message.Length == 0)
            {
                MessageBox.Show("Zadajte prosím správu alebo vložte súbor.",
                    "Chýbajúca správa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                using (SaveFileDialog saveMsgDialog = new SaveFileDialog())
                {
                    if (keysPath.Length != 0)
                        saveMsgDialog.InitialDirectory = keysPath;
                    else
                        saveMsgDialog.InitialDirectory = "c:\\";

                    saveMsgDialog.Title = "Uložte správu";
                    saveMsgDialog.Filter = "msg files (*.msg)|*.msg";
                    saveMsgDialog.FileName = "sprava";

                    if (saveMsgDialog.ShowDialog() == DialogResult.OK)
                    {
                        string msgPath = Path.GetFullPath(saveMsgDialog.FileName);
                        File.WriteAllText(msgPath, message);
                        MessageBox.Show(
                        "Správa bola úspešne uložená." +
                        keysPath, "Správa uložená",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void InsertFileButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openMsgDialog = new OpenFileDialog())
            {
                if (keysPath.Length != 0)
                    openMsgDialog.InitialDirectory = keysPath;
                else
                    openMsgDialog.InitialDirectory = "c:\\";

                openMsgDialog.Title = "Vložte súbor";
                openMsgDialog.Multiselect = false;
                openMsgDialog.Filter = "msg files (*.msg)|*.msg|txt files (*.txt)|*.txt";

                if (openMsgDialog.ShowDialog() == DialogResult.OK)
                {
                    string filePath = openMsgDialog.FileName;
                    string fileContent = File.ReadAllText(filePath);

                    MessageRTBox.Text = fileContent;
                    MessageBox.Show(
                        "Súbor bol úspešne vložený.",
                        "Súbor vložený",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    FileNameValueLabel.Text = openMsgDialog.SafeFileName;
                    DateTime creationDateTime = File.GetCreationTime(filePath);
                    CreationDateValueLabel.Text = creationDateTime.ToString("dd.MM.yyyy");
                    AbsPathValueLabel.Text = filePath;
                    FileTypeValueLabel.Text = Path.GetExtension(filePath);

                    DSA.SetMessagePath(filePath);
                }
            }
        }

        private void SaveSignatureButton_Click(object sender, EventArgs e)
        {
            if (DSA.MessagePath == null)
            {
                MessageBox.Show(
                        "Vložte prosím najprv súbor k podpisu.",
                        "Súbor neexistuje",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
            }
            else
            {
                string digest = DSA.GenerateFileSignature();
                SignatureRTBox.Text = digest;

                using (SaveFileDialog saveSignDialog = new SaveFileDialog())
                {
                    if (keysPath.Length != 0)
                        saveSignDialog.InitialDirectory = keysPath;
                    else
                        saveSignDialog.InitialDirectory = "c:\\";

                    saveSignDialog.Title = "Uložte podpis";
                    saveSignDialog.Filter = "sign files (*.sign)|*.sign";
                    saveSignDialog.FileName = "podpis";

                    if (saveSignDialog.ShowDialog() == DialogResult.OK)
                    {
                        string signPath = Path.GetFullPath(saveSignDialog.FileName);
                        if (DSA.DataToFile(signPath, digest))
                        {
                            MessageBox.Show("Podpis bol uložený do súboru.",
                                "Podpis uložený",
                                MessageBoxButtons.OK, MessageBoxIcon.Information
                                );
                        }
                        else
                        {
                            MessageBox.Show("Podpis nebol uložený do súboru, skúste znovu.",
                                "Chyba ukladania podpisu",
                                MessageBoxButtons.OK, MessageBoxIcon.Error
                                );
                        }
                    }
                }
            }
        }

        private void VerifySignatureButton_Click(object sender, EventArgs e)
        {
            string message = MessageRTBox.Text;
            string signature = SignatureRTBox.Text;

            try
            {
                Cipher.SetEncryptedMessage(
                    new BigInteger[] { Cipher.TextToBigInt(signature) }
                );
            }
            catch
            {
                MessageBox.Show("Podpis nebol verifikovaný!",
                                "Upozornenie",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning
                                );
                return;
            }

            // set enc.message to signature
            

            if (message.Length == 0 || signature.Length == 0)
            {
                MessageBox.Show("Správa alebo podpis chýba, doplňte prosím údaje.",
                                "Chyba overenia",
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Information
                                );
                return;
            }
            else
            {
                if (DSA.SignatureVerified(message))
                {
                    MessageBox.Show("Podpis bol úspešne overený.",
                                "Overenie podpisu",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information
                                );
                }
                else
                {
                    MessageBox.Show("Podpis nie je v poriadku!",
                                "Upozornenie",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning
                                );
                }
            }
        }

        private void ZipButton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveZipDialog = new SaveFileDialog())
            {
                if (keysPath.Length != 0)
                    saveZipDialog.InitialDirectory = keysPath;

                saveZipDialog.Title = "Uložte zálohu";
                saveZipDialog.Filter = "zip files (*.zip)|*.zip";
                saveZipDialog.FileName = "podpis_zaloha";

                if (saveZipDialog.ShowDialog() == DialogResult.OK)
                {
                    string backupDirPath = Path.GetDirectoryName(saveZipDialog.FileName);
                    string zipName = Path.GetFileNameWithoutExtension(saveZipDialog.FileName);
                    string zipNameWithExt = Path.GetFileName(saveZipDialog.FileName);

                    string dirToZipPath = Path.Combine(backupDirPath, zipName);
                    string zipPath = Path.Combine(backupDirPath, zipNameWithExt);
                    Directory.CreateDirectory(dirToZipPath);

                    string msgPath = Path.Combine(dirToZipPath, "sprava.msg");
                    string signPath = Path.Combine(dirToZipPath, "podpis.sign");

                    try
                    {
                        DSA.DataToFile(msgPath, MessageRTBox.Text);
                        DSA.DataToFile(signPath, SignatureRTBox.Text);

                        ZipFile.CreateFromDirectory(dirToZipPath, zipPath);
                        Directory.Delete(dirToZipPath, true);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Nastala chyba " + exc.Message,
                            "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void UnzipFiles()
        {

        }

        private void UnzipButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openZipDialog = new OpenFileDialog())
            {
                if (keysPath.Length != 0)
                    openZipDialog.InitialDirectory = keysPath;
                else
                    openZipDialog.InitialDirectory = "c:\\";

                openZipDialog.Title = "Vyberte zálohu pre dekomprimáciu";
                openZipDialog.Multiselect = false;
                openZipDialog.Filter = "zip files (*.zip)|*.zip";

                if (openZipDialog.ShowDialog() == DialogResult.OK)
                {
                    string zipPath = openZipDialog.FileName;
                    string unzipName = 
                        Path.GetFileNameWithoutExtension(openZipDialog.FileName); 

                    string backupDirPath = 
                        Path.GetDirectoryName(openZipDialog.FileName);

                    string dirToUnzipPath = Path.Combine(backupDirPath, unzipName);
                    
                    try
                    {
                        if (!Directory.Exists(dirToUnzipPath))
                        {
                            Directory.CreateDirectory(dirToUnzipPath);
                            ZipFile.ExtractToDirectory(zipPath, dirToUnzipPath);
                        }
                        else
                        {
                            MessageBox.Show($"Použije sa existujúci priečinok '{unzipName}'.",
                                "Priečinok existuje",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information
                                );
                        }
                        
                        string[] msgFiles = Directory.GetFiles(dirToUnzipPath, "*.msg");
                        string[] signFiles = Directory.GetFiles(dirToUnzipPath, "*.sign");

                        MessageRTBox.Text = DSA.GetMessageFromFile(msgFiles[0]);
                        SignatureRTBox.Text = DSA.GetMessageFromFile(signFiles[0]);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Nastala chyba " + exc.Message,
                            "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
