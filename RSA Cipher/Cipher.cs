﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Bint = System.Numerics.BigInteger;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Collections;
using System.Collections.Concurrent;

namespace RSA_Cipher
{
    static class Cipher
    {
        public static Encoding CentralEUencoding { get; } = Encoding.GetEncoding(852);

        public static List<int> smallPrimes = new List<int>();
        public static int maxBlockToCrypt = 128; 
        public static int maxTextBlokSize = 86;
        public static int KeyBitSize { get; private set; }
        public static void SetKeyBitSize(int keyBitSize) => KeyBitSize = keyBitSize;

        // standard crypting variables
        public static Bint n;
        public static Bint e;
        private static Bint d;
        private static Bint p;
        private static Bint q;
        public const int fixedPublicExp = 65537;
        private const string blockFiller = " ";

        // precomputed CRT variables
        private static Bint dP;
        private static Bint dQ;
        private static Bint qInv;

        public static Bint[] EncryptedMessage { get; private set; }

        public static void SetEncryptedMessage(Bint[] encryptedMessage) 
            => EncryptedMessage = encryptedMessage;

        public static Bint IsPrimeCheckCount = 0; // for diagnostic purposes
        public static Bint GetPrivateExponent() => d;
        public static Bint SetPrivateExponent(Bint privExp) => d = privExp;

        public static bool IsPrimeByDivisionCheck(int number)
        {
            if (number <= 1)
                return false;
            if (number <= 3)
                return true;

            if (number % 2 == 0 || number % 3 == 0) 
                return false;

            for (int i = 5; i * i <= number; i += 6)
            {
                if (number % i == 0 || number % (i + 2) == 0)
                    return false;
            }
            return true;
        }

        public static bool IsPrime(Bint n, int k, byte[] randomBytes)
        {
            if (n % 2 == 0) return false;

            int s = 0;
            Bint d = n - 1;

            while (d % 2 == 0)
            {
                s += 1;
                d /= 2;
            }

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            for (int i = 0; i < k; i++)
            {
                Bint a = 2;
                if (i > 0)
                    a = GenerateRandomBigInt(rng, randomBytes, 2, n - 1);

                Bint x = Bint.ModPow(a, d, n);

                if (x == 1)
                    continue;

                bool probablyPrime = false;
                for (int j = 0; j < s; j++)
                {
                    if (x == n - 1)
                    {
                        probablyPrime = true;
                        break;
                    }
                       
                    x = Bint.Pow(x, 2) % n;
                }
                if (!probablyPrime)
                    return false;
            }
            return true;
        }

        public static void GenerateSmallPrimes()
        {
            for (int i = 0; i < 100000; i++)
            {
                if (IsPrimeByDivisionCheck(i))
                    smallPrimes.Add(i);
            }
        }

        private static bool IsPrimePreCheck(Bint numberToPreCheck)
        {
            int smallPrimesCount = smallPrimes.Count;
            for (int i = 0; i < smallPrimesCount; i++)
            {
                if (numberToPreCheck % smallPrimes[i] == 0)
                    return false;
            }
            return true;
        }

        private static Bint GenerateCandidate(RNGCryptoServiceProvider rng, byte[] randomBytes)
        {
            rng.GetBytes(randomBytes);
            Bint candidate = Bint.Abs(new Bint(randomBytes));

            if (candidate % 2 == 0)
                candidate++;

            Bint candidateLastDigit = candidate % 10;

            if (candidateLastDigit == 3 || candidateLastDigit == 5)
                candidate += 2;

            return candidate;
        }

        private static void GeneratePrime(RNGCryptoServiceProvider rng, int bitSize, ref Bint variable)
        {
            byte[] randomBytes = new byte[bitSize / 8];
            int certainty = (bitSize / 512) * 4; // t=3 iter. for 1028 bit N / impl. in OpenSSL 
            // (1 it res. for base a = 2 - speedup precheck discovers composites quickly) 
            // link: http://cacr.uwaterloo.ca/hac/about/chap4.pdf 
            // Handbook of applied cryptography s. 22, Note 4.63

            Bint candidate;
            --IsPrimeCheckCount;
            do
            {
                candidate = GenerateCandidate(rng, randomBytes);
                while (!IsPrimePreCheck(candidate))
                {
                    candidate = GenerateCandidate(rng, randomBytes);
                }
                ++IsPrimeCheckCount;
            } while (!IsPrime(candidate, certainty, randomBytes));

            Debug.WriteLine("Prime generated. Num of checks: {0}", IsPrimeCheckCount);
            IsPrimeCheckCount = 0;
            variable = candidate;
        }

        private static Bint GenerateRandomBigInt(RNGCryptoServiceProvider rng, byte[] randomBytes, Bint min, Bint max)
        {
            Bint diff = Bint.Subtract(max, min);
            
            rng.GetBytes(randomBytes);
            Bint randomBigInt = Bint.Abs(new Bint(randomBytes));
            Bint mod = randomBigInt % diff;
            randomBigInt = Bint.Add(min, mod);

            return randomBigInt;
        }

        private static Bint GeneratePublicExponent(
            RNGCryptoServiceProvider rng, Bint p, Bint q)
        {
            byte[] randomBytes = new byte[6]; 
            Bint eulerFunction = (p - 1) * (q - 1);
            Bint candidate;

            Bint min = 3;
            Bint max = eulerFunction - 1;

            do
            {
                candidate = GenerateRandomBigInt(rng, randomBytes, min, max);

            } while (!(candidate > 1 && candidate < eulerFunction && Bint.GreatestCommonDivisor(candidate, eulerFunction) == 1));

            return candidate;
        }

        private static bool FermatFactorPossible(Bint p, Bint q, Bint n)
        {
            if (Bint.Pow(Bint.Abs(p-q), 4) < 16 * n)
                return true;
            return false;
        }

        private static Bint GCD(Bint a, Bint b)
        {
            Bint abs_a = Bint.Abs(a);
            Bint abs_b = Bint.Abs(b);

            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            if (a == 0)
                return b;
            else
                return a;
        }

        public static Bint MMI(Bint a, Bint m)
        {
            Bint m0 = m;
            Bint y = 0, x = 1;

            if (m == 1)
                return 0;

            while (a > 1)
            {
                Bint q = a / m;
                Bint t = m;

                m = a % m;
                a = t;
                t = y;

                y = x - q * y;
                x = t;
            }

            if (x < 0)
                x += m0;

            return x;
        }

        public static void CalculateCRTVariables()
        {
            dP = MMI(e, p - 1) % (p - 1);
            dQ = MMI(e, q - 1) % (q - 1);
            qInv = MMI(q, p) % p;
        }

        public static void GenerateKeys(bool isExpFixedSize)
        {
            var primeBitSize = KeyBitSize / 2;
            var rng = new RNGCryptoServiceProvider();

            Stopwatch sw = new Stopwatch();

            Parallel.Invoke(
                () => 
                { GeneratePrime(rng, primeBitSize, ref p); },
                () => 
                { GeneratePrime(rng, primeBitSize, ref q); }
                );

            n = p * q;

            // prevent fermat factorisation - when primes too close generate new q
            while (p == q || FermatFactorPossible(p, q, n))
            {
                GeneratePrime(rng, primeBitSize, ref q);
                n = p * q;
            }

            Bint eulerFunction = (p - 1) * (q - 1);

            // set public exponent
            e = isExpFixedSize ? fixedPublicExp : GeneratePublicExponent(rng, p, q);

            // set private exponent
            d = MMI(e, eulerFunction);

            // calculate CRT variables
            CalculateCRTVariables();
        }

        public static Bint TextToBigInt(string number)
        {
            return Bint.Parse(number);
        }

        public static string NumberBlocksToText(Bint[] numbers)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var num in numbers)
            {
                byte[] chunkBytes = num.ToByteArray();
                sb.Append(CentralEUencoding.GetString(chunkBytes));
            }
            return sb.ToString();
        }

        public static Bint EncryptBlock(string paddedTextBlock)
        {
            byte[] signedPTBlocks = CentralEUencoding.GetBytes(paddedTextBlock);
            Bint m = new Bint(signedPTBlocks);

            return Bint.ModPow(m, e, n);
        }
        
        public static string DecryptBlock(Bint c)
        {
            Bint mDecrypted;
            if (KeyBitSize < 2048 || p == null || q == null)
            {
                mDecrypted = Bint.ModPow(c, d, n);
            }
            else
            {
                // CRT calculation -- lower modular exponentiation (faster decryption)
                Bint m1 = Bint.ModPow(c, dP, p);
                Bint m2 = Bint.ModPow(c, dQ, q);

                Bint h = qInv * (m1 - m2 + p) % p;

                mDecrypted = m2 + (h * q);
            }

            byte[] decryptedByteArray = mDecrypted.ToByteArray();

            return CentralEUencoding.GetString(decryptedByteArray);
        }

        private static string AppendTrailingFillers(string message)
        {
            int fillSize = 0;

            if (message.Length <= maxTextBlokSize)
                fillSize = maxTextBlokSize - message.Length;
            else
                fillSize = maxTextBlokSize - (message.Length % maxTextBlokSize);

            StringBuilder filledMessage = new StringBuilder();
            filledMessage.Append(message);
            
            for (int i = 0; i < fillSize; i++)
            {
                filledMessage.Append(blockFiller);
            }

            return filledMessage.ToString();
        }

        private static string[] SplitMessageToBlocks(string filledMsg)
        {
            string[] textBlocksArray = new string[filledMsg.Length / maxTextBlokSize];
            StringBuilder block = new StringBuilder();
            int blockOrder = 0;

            for (int i = 0; i < filledMsg.Length; i++)
            {
                block.Append(filledMsg[i]);

                if ((i + 1) % maxTextBlokSize == 0)
                {
                    textBlocksArray[blockOrder++] = block.ToString();
                    block.Clear();
                }
            }
            return textBlocksArray;
        }

        private static void AppendRandPartToBlock(ref string textBlock)
        {
            StringBuilder randomizedBlock = new StringBuilder();
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[41];

            rng.GetBytes(randomBytes);
            randomizedBlock.Append(textBlock);
            randomizedBlock.Append(CentralEUencoding.GetString(randomBytes));
            textBlock = randomizedBlock.ToString();
            rng.Dispose();
        }

        private static string BitXorInputs(string input1, string input2)
        {
            StringBuilder xorOutput = new StringBuilder();
            
            byte[] inp1bytes = CentralEUencoding.GetBytes(input1);
            byte[] inp2bytes = CentralEUencoding.GetBytes(input2);
            
            BitArray inp1bits = new BitArray(inp1bytes);
            BitArray inp2bits = new BitArray(inp2bytes);

            inp1bits.Xor(inp2bits);
            byte[] outputBytes = new byte[inp1bits.Length / 8];
            inp1bits.CopyTo(outputBytes, 0);

            return CentralEUencoding.GetString(outputBytes);
        }

        // generate hash for function G, H
        private static string GetHashSlice(int sliceSize, string input)
        {
            StringBuilder hash = new StringBuilder();

            using (SHA512 sha512Hash = SHA512.Create())
            {
                Encoding enc = CentralEUencoding;
                byte[] bytes64 = sha512Hash.ComputeHash(enc.GetBytes(input));
                string firstHash = Encoding.UTF8.GetString(bytes64);

                byte[] bytes64derived = sha512Hash.ComputeHash(enc.GetBytes(firstHash));
                byte[] outPutBytes = new byte[128];

                Buffer.BlockCopy(bytes64, 0, outPutBytes, 0, 64);
                Buffer.BlockCopy(bytes64derived, 0, outPutBytes, 64, 64);

                foreach (var b in outPutBytes)
                {
                    hash.Append(b.ToString("x2"));
                }
            }
            return hash.ToString().Substring(0, sliceSize);
        }

        // own OEAP implementation
        public static string[] ApplyBlockPadding(string message)
        {
            string filledMsg = AppendTrailingFillers(message);
            string[] textBlocksArray = SplitMessageToBlocks(filledMsg);

            string[] paddedTextBlocks = new string[textBlocksArray.Length];

            for(int i = 0; i < textBlocksArray.Length; i++)
            {
                StringBuilder paddedTextBlock = new StringBuilder();

                string m00 = textBlocksArray[i];

                AppendRandPartToBlock(ref textBlocksArray[i]);

                string r = textBlocksArray[i].Substring(maxTextBlokSize); 

                string Gr = GetHashSlice(m00.Length, r);

                string X = BitXorInputs(m00, Gr);
                
                string HX = GetHashSlice(r.Length, X);

                string Y = BitXorInputs(r, HX);

                paddedTextBlock.Append(X);
                paddedTextBlock.Append(Y);

                paddedTextBlocks[i] = paddedTextBlock.ToString();
            }
            return paddedTextBlocks;
        }

        public static void EncryptMessage(string message)
        {
            string[] paddedTextBlocks = ApplyBlockPadding(message);
            int pTBlockLen = paddedTextBlocks.Length;

            Bint[] encryptedNumBlocks = new Bint[pTBlockLen];

            for (int i = 0; i < pTBlockLen; i++)
            {
                encryptedNumBlocks[i] = EncryptBlock(paddedTextBlocks[i]);
            }

            EncryptedMessage = encryptedNumBlocks;
        }

        public static string DecryptMessage()
        {
            StringBuilder mDecrypted = new StringBuilder();
            ConcurrentDictionary<int, Bint> numberBlocksOrdered = new ConcurrentDictionary<int, Bint>();

            int numsMessLen = EncryptedMessage.Length;
            for (int i = 0; i < numsMessLen; i++)
            {
                numberBlocksOrdered[i] = EncryptedMessage[i];
            }

            string[] decryptedTextBlocks = new string[EncryptedMessage.Length];

            Parallel.ForEach(numberBlocksOrdered, numberBlock =>
                {
                    decryptedTextBlocks[numberBlock.Key] = DecryptBlock(numberBlock.Value);
                }
            );

            foreach (var decryptedBlock in decryptedTextBlocks)
            {
                string X = decryptedBlock.Substring(0, maxTextBlokSize); // first 86 chars (m00)
                string Y = decryptedBlock.Substring(maxTextBlokSize); // (Rpart)

                Debug.WriteLine("X len: {0}", X.Length);
                Debug.WriteLine("Y len: {0}", Y.Length);

                string HX = GetHashSlice(Y.Length, X);
                string r = BitXorInputs(Y, HX);

                string Gr = GetHashSlice(X.Length, r);
                string decodedTextBlock = BitXorInputs(X, Gr); 

                mDecrypted.Append(decodedTextBlock);
            }

            return mDecrypted.ToString();
        }
    }
}
