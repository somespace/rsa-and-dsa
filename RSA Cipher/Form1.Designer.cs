﻿namespace RSA_Cipher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainTabControl = new System.Windows.Forms.TabControl();
            this.KeysTabPage = new System.Windows.Forms.TabPage();
            this.KeyTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.KeyManipulateTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.KeySettingsFLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.BitLengthLabel = new System.Windows.Forms.Label();
            this.BitLengthCBox = new System.Windows.Forms.ComboBox();
            this.ExponentLabel = new System.Windows.Forms.Label();
            this.ExponentCBox = new System.Windows.Forms.ComboBox();
            this.keyManTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.InsertKeysButton = new System.Windows.Forms.Button();
            this.ExportKeysButton = new System.Windows.Forms.Button();
            this.GenerateKeysButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.PrivateKeyGBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.PrivateExponentRTBox = new System.Windows.Forms.RichTextBox();
            this.PrivateExponentLabel = new System.Windows.Forms.Label();
            this.PrivateModulusLabel = new System.Windows.Forms.Label();
            this.PrivateModulRTBox = new System.Windows.Forms.RichTextBox();
            this.PublicKeyGBox = new System.Windows.Forms.GroupBox();
            this.PublicKeyTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.PublicExponentRTBox = new System.Windows.Forms.RichTextBox();
            this.PublicExponentLabel = new System.Windows.Forms.Label();
            this.PublicModulusLabel = new System.Windows.Forms.Label();
            this.PublicModulRTBox = new System.Windows.Forms.RichTextBox();
            this.EncryptionTabPage = new System.Windows.Forms.TabPage();
            this.EncryptionTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.EncryptIOTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.OriginalTextRTBox = new System.Windows.Forms.RichTextBox();
            this.EncryptedTextRTBox = new System.Windows.Forms.RichTextBox();
            this.OriginalTextLabel = new System.Windows.Forms.Label();
            this.EncryptedTextLabel = new System.Windows.Forms.Label();
            this.EncryptManipulateTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.EncryptFLPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.EncryptButton = new System.Windows.Forms.Button();
            this.CopyButton = new System.Windows.Forms.Button();
            this.LeftEncFLPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ClearEncryptTabButton = new System.Windows.Forms.Button();
            this.DecryptionTabPage = new System.Windows.Forms.TabPage();
            this.DecryptionTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.DecryptManipulateTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.InsertButton = new System.Windows.Forms.Button();
            this.DecryptFLPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.DecryptButton = new System.Windows.Forms.Button();
            this.ClearDecryptTabButton = new System.Windows.Forms.Button();
            this.DecryptIOTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.TextToDecryptRTBox = new System.Windows.Forms.RichTextBox();
            this.DecryptedTextRTBox = new System.Windows.Forms.RichTextBox();
            this.TextToDecryptLabel = new System.Windows.Forms.Label();
            this.DecryptedTextLabel = new System.Windows.Forms.Label();
            this.SignatureTabPage = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.MessageRTBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FileOverViewLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.SignatureRTBox = new System.Windows.Forms.RichTextBox();
            this.FileInfoWrapperTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.FileInfoGBox = new System.Windows.Forms.GroupBox();
            this.FileInfoTLPanel = new System.Windows.Forms.TableLayoutPanel();
            this.AbsPathValueLabel = new System.Windows.Forms.Label();
            this.FileTypeLabel = new System.Windows.Forms.Label();
            this.FileNameLabel = new System.Windows.Forms.Label();
            this.AbsPathLabel = new System.Windows.Forms.Label();
            this.CreationDateLabel = new System.Windows.Forms.Label();
            this.CreationDateValueLabel = new System.Windows.Forms.Label();
            this.FileNameValueLabel = new System.Windows.Forms.Label();
            this.FileTypeValueLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.UnzipButton = new System.Windows.Forms.Button();
            this.ZipButton = new System.Windows.Forms.Button();
            this.VerifySignatureButton = new System.Windows.Forms.Button();
            this.MessageManipulateFLPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SaveMessageButton = new System.Windows.Forms.Button();
            this.InsertFileButton = new System.Windows.Forms.Button();
            this.SaveSignatureButton = new System.Windows.Forms.Button();
            this.MainTabControl.SuspendLayout();
            this.KeysTabPage.SuspendLayout();
            this.KeyTLPanel.SuspendLayout();
            this.KeyManipulateTLPanel.SuspendLayout();
            this.KeySettingsFLayout.SuspendLayout();
            this.keyManTLPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.PrivateKeyGBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.PublicKeyGBox.SuspendLayout();
            this.PublicKeyTLPanel.SuspendLayout();
            this.EncryptionTabPage.SuspendLayout();
            this.EncryptionTLPanel.SuspendLayout();
            this.EncryptIOTLPanel.SuspendLayout();
            this.EncryptManipulateTLPanel.SuspendLayout();
            this.EncryptFLPanel.SuspendLayout();
            this.LeftEncFLPanel.SuspendLayout();
            this.DecryptionTabPage.SuspendLayout();
            this.DecryptionTLPanel.SuspendLayout();
            this.DecryptManipulateTLPanel.SuspendLayout();
            this.DecryptFLPanel.SuspendLayout();
            this.DecryptIOTLPanel.SuspendLayout();
            this.SignatureTabPage.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.FileInfoWrapperTLPanel.SuspendLayout();
            this.FileInfoGBox.SuspendLayout();
            this.FileInfoTLPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.MessageManipulateFLPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainTabControl
            // 
            this.MainTabControl.Controls.Add(this.KeysTabPage);
            this.MainTabControl.Controls.Add(this.EncryptionTabPage);
            this.MainTabControl.Controls.Add(this.DecryptionTabPage);
            this.MainTabControl.Controls.Add(this.SignatureTabPage);
            this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainTabControl.HotTrack = true;
            this.MainTabControl.ItemSize = new System.Drawing.Size(110, 25);
            this.MainTabControl.Location = new System.Drawing.Point(5, 15);
            this.MainTabControl.Margin = new System.Windows.Forms.Padding(0);
            this.MainTabControl.Multiline = true;
            this.MainTabControl.Name = "MainTabControl";
            this.MainTabControl.SelectedIndex = 0;
            this.MainTabControl.Size = new System.Drawing.Size(957, 636);
            this.MainTabControl.TabIndex = 0;
            // 
            // KeysTabPage
            // 
            this.KeysTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.KeysTabPage.Controls.Add(this.KeyTLPanel);
            this.KeysTabPage.Location = new System.Drawing.Point(4, 29);
            this.KeysTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.KeysTabPage.Name = "KeysTabPage";
            this.KeysTabPage.Size = new System.Drawing.Size(949, 603);
            this.KeysTabPage.TabIndex = 0;
            this.KeysTabPage.Text = "Kľúče";
            // 
            // KeyTLPanel
            // 
            this.KeyTLPanel.BackColor = System.Drawing.SystemColors.Control;
            this.KeyTLPanel.ColumnCount = 1;
            this.KeyTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.KeyTLPanel.Controls.Add(this.KeyManipulateTLPanel, 0, 1);
            this.KeyTLPanel.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.KeyTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyTLPanel.Location = new System.Drawing.Point(0, 0);
            this.KeyTLPanel.Name = "KeyTLPanel";
            this.KeyTLPanel.RowCount = 2;
            this.KeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.KeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.KeyTLPanel.Size = new System.Drawing.Size(949, 603);
            this.KeyTLPanel.TabIndex = 0;
            // 
            // KeyManipulateTLPanel
            // 
            this.KeyManipulateTLPanel.ColumnCount = 2;
            this.KeyManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.KeyManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.KeyManipulateTLPanel.Controls.Add(this.KeySettingsFLayout, 0, 0);
            this.KeyManipulateTLPanel.Controls.Add(this.keyManTLPanel, 1, 0);
            this.KeyManipulateTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeyManipulateTLPanel.Location = new System.Drawing.Point(0, 528);
            this.KeyManipulateTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.KeyManipulateTLPanel.Name = "KeyManipulateTLPanel";
            this.KeyManipulateTLPanel.RowCount = 1;
            this.KeyManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.KeyManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.KeyManipulateTLPanel.Size = new System.Drawing.Size(949, 75);
            this.KeyManipulateTLPanel.TabIndex = 3;
            // 
            // KeySettingsFLayout
            // 
            this.KeySettingsFLayout.Controls.Add(this.BitLengthLabel);
            this.KeySettingsFLayout.Controls.Add(this.BitLengthCBox);
            this.KeySettingsFLayout.Controls.Add(this.ExponentLabel);
            this.KeySettingsFLayout.Controls.Add(this.ExponentCBox);
            this.KeySettingsFLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KeySettingsFLayout.Location = new System.Drawing.Point(0, 0);
            this.KeySettingsFLayout.Margin = new System.Windows.Forms.Padding(0);
            this.KeySettingsFLayout.Name = "KeySettingsFLayout";
            this.KeySettingsFLayout.Size = new System.Drawing.Size(474, 75);
            this.KeySettingsFLayout.TabIndex = 2;
            // 
            // BitLengthLabel
            // 
            this.BitLengthLabel.AutoSize = true;
            this.BitLengthLabel.Location = new System.Drawing.Point(35, 24);
            this.BitLengthLabel.Margin = new System.Windows.Forms.Padding(35, 24, 5, 0);
            this.BitLengthLabel.Name = "BitLengthLabel";
            this.BitLengthLabel.Size = new System.Drawing.Size(39, 20);
            this.BitLengthLabel.TabIndex = 0;
            this.BitLengthLabel.Text = "Bity:";
            // 
            // BitLengthCBox
            // 
            this.BitLengthCBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.BitLengthCBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.BitLengthCBox.BackColor = System.Drawing.Color.White;
            this.BitLengthCBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::RSA_Cipher.Properties.Settings.Default, "default_bitsize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BitLengthCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BitLengthCBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BitLengthCBox.FormattingEnabled = true;
            this.BitLengthCBox.Items.AddRange(new object[] {
            "1024",
            "2048",
            "3072",
            "4096"});
            this.BitLengthCBox.Location = new System.Drawing.Point(79, 20);
            this.BitLengthCBox.Margin = new System.Windows.Forms.Padding(0, 20, 0, 3);
            this.BitLengthCBox.MaxDropDownItems = 4;
            this.BitLengthCBox.Name = "BitLengthCBox";
            this.BitLengthCBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.BitLengthCBox.Size = new System.Drawing.Size(80, 28);
            this.BitLengthCBox.Sorted = true;
            this.BitLengthCBox.TabIndex = 3;
            this.BitLengthCBox.Text = global::RSA_Cipher.Properties.Settings.Default.default_bitsize;
            // 
            // ExponentLabel
            // 
            this.ExponentLabel.AutoSize = true;
            this.ExponentLabel.Location = new System.Drawing.Point(184, 24);
            this.ExponentLabel.Margin = new System.Windows.Forms.Padding(25, 24, 5, 0);
            this.ExponentLabel.Name = "ExponentLabel";
            this.ExponentLabel.Size = new System.Drawing.Size(95, 20);
            this.ExponentLabel.TabIndex = 4;
            this.ExponentLabel.Text = "Verejný exp:";
            // 
            // ExponentCBox
            // 
            this.ExponentCBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.ExponentCBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ExponentCBox.BackColor = System.Drawing.Color.White;
            this.ExponentCBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::RSA_Cipher.Properties.Settings.Default, "default_bitsize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ExponentCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ExponentCBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ExponentCBox.FormattingEnabled = true;
            this.ExponentCBox.Items.AddRange(new object[] {
            "Fixný (65537)",
            "Premenlivý"});
            this.ExponentCBox.Location = new System.Drawing.Point(287, 20);
            this.ExponentCBox.Margin = new System.Windows.Forms.Padding(3, 20, 2, 3);
            this.ExponentCBox.MaxDropDownItems = 4;
            this.ExponentCBox.Name = "ExponentCBox";
            this.ExponentCBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ExponentCBox.Size = new System.Drawing.Size(138, 28);
            this.ExponentCBox.Sorted = true;
            this.ExponentCBox.TabIndex = 1;
            this.ExponentCBox.Text = global::RSA_Cipher.Properties.Settings.Default.default_bitsize;
            // 
            // keyManTLPanel
            // 
            this.keyManTLPanel.ColumnCount = 3;
            this.keyManTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.keyManTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.keyManTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.keyManTLPanel.Controls.Add(this.InsertKeysButton, 0, 0);
            this.keyManTLPanel.Controls.Add(this.ExportKeysButton, 1, 0);
            this.keyManTLPanel.Controls.Add(this.GenerateKeysButton, 0, 0);
            this.keyManTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keyManTLPanel.Location = new System.Drawing.Point(477, 3);
            this.keyManTLPanel.Name = "keyManTLPanel";
            this.keyManTLPanel.RowCount = 1;
            this.keyManTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.keyManTLPanel.Size = new System.Drawing.Size(469, 69);
            this.keyManTLPanel.TabIndex = 3;
            // 
            // InsertKeysButton
            // 
            this.InsertKeysButton.Location = new System.Drawing.Point(171, 15);
            this.InsertKeysButton.Margin = new System.Windows.Forms.Padding(15, 15, 15, 3);
            this.InsertKeysButton.Name = "InsertKeysButton";
            this.InsertKeysButton.Size = new System.Drawing.Size(126, 40);
            this.InsertKeysButton.TabIndex = 3;
            this.InsertKeysButton.Text = "Vlož";
            this.InsertKeysButton.UseVisualStyleBackColor = true;
            this.InsertKeysButton.Click += new System.EventHandler(this.InsertKeysButton_Click);
            // 
            // ExportKeysButton
            // 
            this.ExportKeysButton.Location = new System.Drawing.Point(312, 15);
            this.ExportKeysButton.Margin = new System.Windows.Forms.Padding(0, 15, 35, 3);
            this.ExportKeysButton.Name = "ExportKeysButton";
            this.ExportKeysButton.Size = new System.Drawing.Size(122, 40);
            this.ExportKeysButton.TabIndex = 2;
            this.ExportKeysButton.Text = "Exportuj";
            this.ExportKeysButton.UseVisualStyleBackColor = true;
            this.ExportKeysButton.Click += new System.EventHandler(this.ExportKeysButton_Click);
            // 
            // GenerateKeysButton
            // 
            this.GenerateKeysButton.Location = new System.Drawing.Point(35, 15);
            this.GenerateKeysButton.Margin = new System.Windows.Forms.Padding(35, 15, 0, 3);
            this.GenerateKeysButton.Name = "GenerateKeysButton";
            this.GenerateKeysButton.Size = new System.Drawing.Size(121, 40);
            this.GenerateKeysButton.TabIndex = 1;
            this.GenerateKeysButton.Text = "Generuj";
            this.GenerateKeysButton.UseVisualStyleBackColor = true;
            this.GenerateKeysButton.Click += new System.EventHandler(this.GenerateKeysButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.PrivateKeyGBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.PublicKeyGBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(949, 528);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // PrivateKeyGBox
            // 
            this.PrivateKeyGBox.Controls.Add(this.tableLayoutPanel2);
            this.PrivateKeyGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrivateKeyGBox.Location = new System.Drawing.Point(489, 20);
            this.PrivateKeyGBox.Margin = new System.Windows.Forms.Padding(15, 20, 15, 0);
            this.PrivateKeyGBox.Name = "PrivateKeyGBox";
            this.PrivateKeyGBox.Size = new System.Drawing.Size(445, 508);
            this.PrivateKeyGBox.TabIndex = 1;
            this.PrivateKeyGBox.TabStop = false;
            this.PrivateKeyGBox.Text = "Súkromný kľúč";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.PrivateExponentRTBox, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.PrivateExponentLabel, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.PrivateModulusLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.PrivateModulRTBox, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(439, 483);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // PrivateExponentRTBox
            // 
            this.PrivateExponentRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrivateExponentRTBox.Location = new System.Drawing.Point(20, 286);
            this.PrivateExponentRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 15);
            this.PrivateExponentRTBox.Name = "PrivateExponentRTBox";
            this.PrivateExponentRTBox.Size = new System.Drawing.Size(399, 177);
            this.PrivateExponentRTBox.TabIndex = 4;
            this.PrivateExponentRTBox.Text = "";
            // 
            // PrivateExponentLabel
            // 
            this.PrivateExponentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PrivateExponentLabel.AutoSize = true;
            this.PrivateExponentLabel.Location = new System.Drawing.Point(15, 256);
            this.PrivateExponentLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.PrivateExponentLabel.Name = "PrivateExponentLabel";
            this.PrivateExponentLabel.Size = new System.Drawing.Size(173, 20);
            this.PrivateExponentLabel.TabIndex = 3;
            this.PrivateExponentLabel.Text = "Súkromný exponent (d)";
            // 
            // PrivateModulusLabel
            // 
            this.PrivateModulusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PrivateModulusLabel.AutoSize = true;
            this.PrivateModulusLabel.Location = new System.Drawing.Point(15, 20);
            this.PrivateModulusLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.PrivateModulusLabel.Name = "PrivateModulusLabel";
            this.PrivateModulusLabel.Size = new System.Drawing.Size(75, 20);
            this.PrivateModulusLabel.TabIndex = 0;
            this.PrivateModulusLabel.Text = "Modul (n)";
            // 
            // PrivateModulRTBox
            // 
            this.PrivateModulRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrivateModulRTBox.Location = new System.Drawing.Point(20, 50);
            this.PrivateModulRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 15);
            this.PrivateModulRTBox.Name = "PrivateModulRTBox";
            this.PrivateModulRTBox.Size = new System.Drawing.Size(399, 176);
            this.PrivateModulRTBox.TabIndex = 2;
            this.PrivateModulRTBox.Text = "";
            // 
            // PublicKeyGBox
            // 
            this.PublicKeyGBox.Controls.Add(this.PublicKeyTLPanel);
            this.PublicKeyGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PublicKeyGBox.Location = new System.Drawing.Point(15, 20);
            this.PublicKeyGBox.Margin = new System.Windows.Forms.Padding(15, 20, 15, 0);
            this.PublicKeyGBox.Name = "PublicKeyGBox";
            this.PublicKeyGBox.Size = new System.Drawing.Size(444, 508);
            this.PublicKeyGBox.TabIndex = 0;
            this.PublicKeyGBox.TabStop = false;
            this.PublicKeyGBox.Text = "Verejný kľúč";
            // 
            // PublicKeyTLPanel
            // 
            this.PublicKeyTLPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PublicKeyTLPanel.ColumnCount = 1;
            this.PublicKeyTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PublicKeyTLPanel.Controls.Add(this.PublicExponentRTBox, 0, 3);
            this.PublicKeyTLPanel.Controls.Add(this.PublicExponentLabel, 0, 2);
            this.PublicKeyTLPanel.Controls.Add(this.PublicModulusLabel, 0, 0);
            this.PublicKeyTLPanel.Controls.Add(this.PublicModulRTBox, 0, 1);
            this.PublicKeyTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PublicKeyTLPanel.Location = new System.Drawing.Point(3, 22);
            this.PublicKeyTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.PublicKeyTLPanel.Name = "PublicKeyTLPanel";
            this.PublicKeyTLPanel.Padding = new System.Windows.Forms.Padding(5);
            this.PublicKeyTLPanel.RowCount = 4;
            this.PublicKeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.PublicKeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.PublicKeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.PublicKeyTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.PublicKeyTLPanel.Size = new System.Drawing.Size(438, 483);
            this.PublicKeyTLPanel.TabIndex = 0;
            // 
            // PublicExponentRTBox
            // 
            this.PublicExponentRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PublicExponentRTBox.Location = new System.Drawing.Point(20, 286);
            this.PublicExponentRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 15);
            this.PublicExponentRTBox.Name = "PublicExponentRTBox";
            this.PublicExponentRTBox.Size = new System.Drawing.Size(398, 177);
            this.PublicExponentRTBox.TabIndex = 4;
            this.PublicExponentRTBox.Text = "";
            // 
            // PublicExponentLabel
            // 
            this.PublicExponentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PublicExponentLabel.AutoSize = true;
            this.PublicExponentLabel.Location = new System.Drawing.Point(15, 256);
            this.PublicExponentLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.PublicExponentLabel.Name = "PublicExponentLabel";
            this.PublicExponentLabel.Size = new System.Drawing.Size(155, 20);
            this.PublicExponentLabel.TabIndex = 3;
            this.PublicExponentLabel.Text = "Verejný exponent (e)";
            // 
            // PublicModulusLabel
            // 
            this.PublicModulusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PublicModulusLabel.AutoSize = true;
            this.PublicModulusLabel.Location = new System.Drawing.Point(15, 20);
            this.PublicModulusLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.PublicModulusLabel.Name = "PublicModulusLabel";
            this.PublicModulusLabel.Size = new System.Drawing.Size(75, 20);
            this.PublicModulusLabel.TabIndex = 0;
            this.PublicModulusLabel.Text = "Modul (n)";
            // 
            // PublicModulRTBox
            // 
            this.PublicModulRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PublicModulRTBox.Location = new System.Drawing.Point(20, 50);
            this.PublicModulRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 15);
            this.PublicModulRTBox.Name = "PublicModulRTBox";
            this.PublicModulRTBox.Size = new System.Drawing.Size(398, 176);
            this.PublicModulRTBox.TabIndex = 2;
            this.PublicModulRTBox.Text = "";
            // 
            // EncryptionTabPage
            // 
            this.EncryptionTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.EncryptionTabPage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.EncryptionTabPage.Controls.Add(this.EncryptionTLPanel);
            this.EncryptionTabPage.Location = new System.Drawing.Point(4, 29);
            this.EncryptionTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.EncryptionTabPage.Name = "EncryptionTabPage";
            this.EncryptionTabPage.Size = new System.Drawing.Size(949, 603);
            this.EncryptionTabPage.TabIndex = 1;
            this.EncryptionTabPage.Text = "Šifrovanie";
            // 
            // EncryptionTLPanel
            // 
            this.EncryptionTLPanel.BackColor = System.Drawing.SystemColors.Control;
            this.EncryptionTLPanel.ColumnCount = 1;
            this.EncryptionTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.EncryptionTLPanel.Controls.Add(this.EncryptIOTLPanel, 0, 0);
            this.EncryptionTLPanel.Controls.Add(this.EncryptManipulateTLPanel, 0, 1);
            this.EncryptionTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EncryptionTLPanel.Location = new System.Drawing.Point(0, 0);
            this.EncryptionTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.EncryptionTLPanel.Name = "EncryptionTLPanel";
            this.EncryptionTLPanel.RowCount = 2;
            this.EncryptionTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.EncryptionTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.EncryptionTLPanel.Size = new System.Drawing.Size(949, 603);
            this.EncryptionTLPanel.TabIndex = 1;
            // 
            // EncryptIOTLPanel
            // 
            this.EncryptIOTLPanel.BackColor = System.Drawing.SystemColors.Control;
            this.EncryptIOTLPanel.ColumnCount = 2;
            this.EncryptIOTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.EncryptIOTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.EncryptIOTLPanel.Controls.Add(this.OriginalTextRTBox, 0, 1);
            this.EncryptIOTLPanel.Controls.Add(this.EncryptedTextRTBox, 1, 1);
            this.EncryptIOTLPanel.Controls.Add(this.OriginalTextLabel, 0, 0);
            this.EncryptIOTLPanel.Controls.Add(this.EncryptedTextLabel, 1, 0);
            this.EncryptIOTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EncryptIOTLPanel.Location = new System.Drawing.Point(0, 0);
            this.EncryptIOTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.EncryptIOTLPanel.Name = "EncryptIOTLPanel";
            this.EncryptIOTLPanel.RowCount = 2;
            this.EncryptIOTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.EncryptIOTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.EncryptIOTLPanel.Size = new System.Drawing.Size(949, 528);
            this.EncryptIOTLPanel.TabIndex = 1;
            // 
            // OriginalTextRTBox
            // 
            this.OriginalTextRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OriginalTextRTBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OriginalTextRTBox.Location = new System.Drawing.Point(15, 50);
            this.OriginalTextRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.OriginalTextRTBox.Name = "OriginalTextRTBox";
            this.OriginalTextRTBox.Size = new System.Drawing.Size(444, 468);
            this.OriginalTextRTBox.TabIndex = 0;
            this.OriginalTextRTBox.Text = "";
            // 
            // EncryptedTextRTBox
            // 
            this.EncryptedTextRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EncryptedTextRTBox.Location = new System.Drawing.Point(489, 50);
            this.EncryptedTextRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.EncryptedTextRTBox.Name = "EncryptedTextRTBox";
            this.EncryptedTextRTBox.Size = new System.Drawing.Size(445, 468);
            this.EncryptedTextRTBox.TabIndex = 1;
            this.EncryptedTextRTBox.Text = "";
            // 
            // OriginalTextLabel
            // 
            this.OriginalTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OriginalTextLabel.AutoSize = true;
            this.OriginalTextLabel.Location = new System.Drawing.Point(10, 20);
            this.OriginalTextLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.OriginalTextLabel.Name = "OriginalTextLabel";
            this.OriginalTextLabel.Size = new System.Drawing.Size(99, 20);
            this.OriginalTextLabel.TabIndex = 2;
            this.OriginalTextLabel.Text = "Pôvodný text";
            this.OriginalTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EncryptedTextLabel
            // 
            this.EncryptedTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EncryptedTextLabel.AutoSize = true;
            this.EncryptedTextLabel.Location = new System.Drawing.Point(484, 20);
            this.EncryptedTextLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.EncryptedTextLabel.Name = "EncryptedTextLabel";
            this.EncryptedTextLabel.Size = new System.Drawing.Size(120, 20);
            this.EncryptedTextLabel.TabIndex = 3;
            this.EncryptedTextLabel.Text = "Zašifrovaný text";
            this.EncryptedTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EncryptManipulateTLPanel
            // 
            this.EncryptManipulateTLPanel.ColumnCount = 2;
            this.EncryptManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.EncryptManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.EncryptManipulateTLPanel.Controls.Add(this.EncryptFLPanel, 1, 0);
            this.EncryptManipulateTLPanel.Controls.Add(this.LeftEncFLPanel, 0, 0);
            this.EncryptManipulateTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EncryptManipulateTLPanel.Location = new System.Drawing.Point(0, 528);
            this.EncryptManipulateTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.EncryptManipulateTLPanel.Name = "EncryptManipulateTLPanel";
            this.EncryptManipulateTLPanel.RowCount = 2;
            this.EncryptManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.EncryptManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.EncryptManipulateTLPanel.Size = new System.Drawing.Size(949, 75);
            this.EncryptManipulateTLPanel.TabIndex = 2;
            // 
            // EncryptFLPanel
            // 
            this.EncryptFLPanel.Controls.Add(this.EncryptButton);
            this.EncryptFLPanel.Controls.Add(this.CopyButton);
            this.EncryptFLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EncryptFLPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.EncryptFLPanel.Location = new System.Drawing.Point(474, 0);
            this.EncryptFLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.EncryptFLPanel.Name = "EncryptFLPanel";
            this.EncryptFLPanel.Size = new System.Drawing.Size(475, 55);
            this.EncryptFLPanel.TabIndex = 2;
            // 
            // EncryptButton
            // 
            this.EncryptButton.Location = new System.Drawing.Point(287, 10);
            this.EncryptButton.Margin = new System.Windows.Forms.Padding(3, 10, 15, 3);
            this.EncryptButton.Name = "EncryptButton";
            this.EncryptButton.Size = new System.Drawing.Size(173, 40);
            this.EncryptButton.TabIndex = 1;
            this.EncryptButton.Text = "Zašifruj";
            this.EncryptButton.UseVisualStyleBackColor = true;
            this.EncryptButton.Click += new System.EventHandler(this.EncryptButton_Click_1);
            // 
            // CopyButton
            // 
            this.CopyButton.Location = new System.Drawing.Point(101, 10);
            this.CopyButton.Margin = new System.Windows.Forms.Padding(15, 10, 10, 3);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(173, 40);
            this.CopyButton.TabIndex = 1;
            this.CopyButton.Text = "Kopíruj";
            this.CopyButton.UseVisualStyleBackColor = true;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // LeftEncFLPanel
            // 
            this.LeftEncFLPanel.Controls.Add(this.ClearEncryptTabButton);
            this.LeftEncFLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftEncFLPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftEncFLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LeftEncFLPanel.Name = "LeftEncFLPanel";
            this.LeftEncFLPanel.Size = new System.Drawing.Size(474, 55);
            this.LeftEncFLPanel.TabIndex = 3;
            // 
            // ClearEncryptTabButton
            // 
            this.ClearEncryptTabButton.Location = new System.Drawing.Point(15, 10);
            this.ClearEncryptTabButton.Margin = new System.Windows.Forms.Padding(15, 10, 10, 3);
            this.ClearEncryptTabButton.Name = "ClearEncryptTabButton";
            this.ClearEncryptTabButton.Size = new System.Drawing.Size(173, 40);
            this.ClearEncryptTabButton.TabIndex = 3;
            this.ClearEncryptTabButton.Text = "Vyčisti formulár";
            this.ClearEncryptTabButton.UseVisualStyleBackColor = true;
            this.ClearEncryptTabButton.Click += new System.EventHandler(this.ClearEncryptTabButton_Click);
            // 
            // DecryptionTabPage
            // 
            this.DecryptionTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.DecryptionTabPage.Controls.Add(this.DecryptionTLPanel);
            this.DecryptionTabPage.Location = new System.Drawing.Point(4, 29);
            this.DecryptionTabPage.Margin = new System.Windows.Forms.Padding(0);
            this.DecryptionTabPage.Name = "DecryptionTabPage";
            this.DecryptionTabPage.Size = new System.Drawing.Size(949, 603);
            this.DecryptionTabPage.TabIndex = 2;
            this.DecryptionTabPage.Text = "Dešifrovanie";
            // 
            // DecryptionTLPanel
            // 
            this.DecryptionTLPanel.ColumnCount = 1;
            this.DecryptionTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DecryptionTLPanel.Controls.Add(this.DecryptManipulateTLPanel, 0, 1);
            this.DecryptionTLPanel.Controls.Add(this.DecryptIOTLPanel, 0, 0);
            this.DecryptionTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecryptionTLPanel.Location = new System.Drawing.Point(0, 0);
            this.DecryptionTLPanel.Name = "DecryptionTLPanel";
            this.DecryptionTLPanel.RowCount = 2;
            this.DecryptionTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DecryptionTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.DecryptionTLPanel.Size = new System.Drawing.Size(949, 603);
            this.DecryptionTLPanel.TabIndex = 1;
            // 
            // DecryptManipulateTLPanel
            // 
            this.DecryptManipulateTLPanel.ColumnCount = 2;
            this.DecryptManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DecryptManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DecryptManipulateTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.DecryptManipulateTLPanel.Controls.Add(this.InsertButton, 0, 0);
            this.DecryptManipulateTLPanel.Controls.Add(this.DecryptFLPanel, 1, 0);
            this.DecryptManipulateTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecryptManipulateTLPanel.Location = new System.Drawing.Point(0, 528);
            this.DecryptManipulateTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DecryptManipulateTLPanel.Name = "DecryptManipulateTLPanel";
            this.DecryptManipulateTLPanel.RowCount = 2;
            this.DecryptManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DecryptManipulateTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.DecryptManipulateTLPanel.Size = new System.Drawing.Size(949, 75);
            this.DecryptManipulateTLPanel.TabIndex = 3;
            // 
            // InsertButton
            // 
            this.InsertButton.Location = new System.Drawing.Point(15, 10);
            this.InsertButton.Margin = new System.Windows.Forms.Padding(15, 10, 3, 3);
            this.InsertButton.Name = "InsertButton";
            this.InsertButton.Size = new System.Drawing.Size(173, 40);
            this.InsertButton.TabIndex = 0;
            this.InsertButton.Text = "Vlož";
            this.InsertButton.UseVisualStyleBackColor = true;
            this.InsertButton.Click += new System.EventHandler(this.InsertButton_Click);
            // 
            // DecryptFLPanel
            // 
            this.DecryptFLPanel.Controls.Add(this.DecryptButton);
            this.DecryptFLPanel.Controls.Add(this.ClearDecryptTabButton);
            this.DecryptFLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecryptFLPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.DecryptFLPanel.Location = new System.Drawing.Point(474, 0);
            this.DecryptFLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DecryptFLPanel.Name = "DecryptFLPanel";
            this.DecryptFLPanel.Size = new System.Drawing.Size(475, 55);
            this.DecryptFLPanel.TabIndex = 2;
            // 
            // DecryptButton
            // 
            this.DecryptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DecryptButton.Location = new System.Drawing.Point(287, 10);
            this.DecryptButton.Margin = new System.Windows.Forms.Padding(3, 10, 15, 3);
            this.DecryptButton.Name = "DecryptButton";
            this.DecryptButton.Size = new System.Drawing.Size(173, 40);
            this.DecryptButton.TabIndex = 1;
            this.DecryptButton.Text = "Dešifruj";
            this.DecryptButton.UseVisualStyleBackColor = true;
            this.DecryptButton.Click += new System.EventHandler(this.DecryptButton_Click);
            // 
            // ClearDecryptTabButton
            // 
            this.ClearDecryptTabButton.Location = new System.Drawing.Point(101, 10);
            this.ClearDecryptTabButton.Margin = new System.Windows.Forms.Padding(15, 10, 10, 3);
            this.ClearDecryptTabButton.Name = "ClearDecryptTabButton";
            this.ClearDecryptTabButton.Size = new System.Drawing.Size(173, 40);
            this.ClearDecryptTabButton.TabIndex = 2;
            this.ClearDecryptTabButton.Text = "Vyčisti form.";
            this.ClearDecryptTabButton.UseVisualStyleBackColor = true;
            this.ClearDecryptTabButton.Click += new System.EventHandler(this.ClearDecryptTabButton_Click);
            // 
            // DecryptIOTLPanel
            // 
            this.DecryptIOTLPanel.BackColor = System.Drawing.SystemColors.Control;
            this.DecryptIOTLPanel.ColumnCount = 2;
            this.DecryptIOTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DecryptIOTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.DecryptIOTLPanel.Controls.Add(this.TextToDecryptRTBox, 0, 1);
            this.DecryptIOTLPanel.Controls.Add(this.DecryptedTextRTBox, 1, 1);
            this.DecryptIOTLPanel.Controls.Add(this.TextToDecryptLabel, 0, 0);
            this.DecryptIOTLPanel.Controls.Add(this.DecryptedTextLabel, 1, 0);
            this.DecryptIOTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecryptIOTLPanel.Location = new System.Drawing.Point(0, 0);
            this.DecryptIOTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DecryptIOTLPanel.Name = "DecryptIOTLPanel";
            this.DecryptIOTLPanel.RowCount = 2;
            this.DecryptIOTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.DecryptIOTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.DecryptIOTLPanel.Size = new System.Drawing.Size(949, 528);
            this.DecryptIOTLPanel.TabIndex = 2;
            // 
            // TextToDecryptRTBox
            // 
            this.TextToDecryptRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextToDecryptRTBox.Location = new System.Drawing.Point(15, 50);
            this.TextToDecryptRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.TextToDecryptRTBox.Name = "TextToDecryptRTBox";
            this.TextToDecryptRTBox.Size = new System.Drawing.Size(444, 468);
            this.TextToDecryptRTBox.TabIndex = 0;
            this.TextToDecryptRTBox.Text = "";
            // 
            // DecryptedTextRTBox
            // 
            this.DecryptedTextRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DecryptedTextRTBox.Location = new System.Drawing.Point(489, 50);
            this.DecryptedTextRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.DecryptedTextRTBox.Name = "DecryptedTextRTBox";
            this.DecryptedTextRTBox.Size = new System.Drawing.Size(445, 468);
            this.DecryptedTextRTBox.TabIndex = 1;
            this.DecryptedTextRTBox.Text = "";
            // 
            // TextToDecryptLabel
            // 
            this.TextToDecryptLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextToDecryptLabel.AutoSize = true;
            this.TextToDecryptLabel.Location = new System.Drawing.Point(10, 20);
            this.TextToDecryptLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.TextToDecryptLabel.Name = "TextToDecryptLabel";
            this.TextToDecryptLabel.Size = new System.Drawing.Size(120, 20);
            this.TextToDecryptLabel.TabIndex = 2;
            this.TextToDecryptLabel.Text = "Zašifrovaný text";
            this.TextToDecryptLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DecryptedTextLabel
            // 
            this.DecryptedTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DecryptedTextLabel.AutoSize = true;
            this.DecryptedTextLabel.Location = new System.Drawing.Point(484, 20);
            this.DecryptedTextLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.DecryptedTextLabel.Name = "DecryptedTextLabel";
            this.DecryptedTextLabel.Size = new System.Drawing.Size(122, 20);
            this.DecryptedTextLabel.TabIndex = 3;
            this.DecryptedTextLabel.Text = "Dešifrovaný text";
            this.DecryptedTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SignatureTabPage
            // 
            this.SignatureTabPage.BackColor = System.Drawing.SystemColors.Control;
            this.SignatureTabPage.Controls.Add(this.tableLayoutPanel3);
            this.SignatureTabPage.Location = new System.Drawing.Point(4, 29);
            this.SignatureTabPage.Name = "SignatureTabPage";
            this.SignatureTabPage.Size = new System.Drawing.Size(949, 603);
            this.SignatureTabPage.TabIndex = 3;
            this.SignatureTabPage.Text = "Podpis";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(949, 603);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.MessageRTBox, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.FileOverViewLabel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel6, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(949, 528);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // MessageRTBox
            // 
            this.MessageRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageRTBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageRTBox.Location = new System.Drawing.Point(15, 50);
            this.MessageRTBox.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.MessageRTBox.Name = "MessageRTBox";
            this.MessageRTBox.Size = new System.Drawing.Size(444, 468);
            this.MessageRTBox.TabIndex = 0;
            this.MessageRTBox.Text = "";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(484, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Podpis:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileOverViewLabel
            // 
            this.FileOverViewLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.FileOverViewLabel.AutoSize = true;
            this.FileOverViewLabel.Location = new System.Drawing.Point(10, 20);
            this.FileOverViewLabel.Margin = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.FileOverViewLabel.Name = "FileOverViewLabel";
            this.FileOverViewLabel.Size = new System.Drawing.Size(118, 20);
            this.FileOverViewLabel.TabIndex = 2;
            this.FileOverViewLabel.Text = "Náhľad súboru:";
            this.FileOverViewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.SignatureRTBox, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.FileInfoWrapperTLPanel, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(474, 40);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.Padding = new System.Windows.Forms.Padding(10, 7, 10, 10);
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(475, 488);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // SignatureRTBox
            // 
            this.SignatureRTBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SignatureRTBox.Location = new System.Drawing.Point(13, 10);
            this.SignatureRTBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.SignatureRTBox.Name = "SignatureRTBox";
            this.SignatureRTBox.Size = new System.Drawing.Size(449, 222);
            this.SignatureRTBox.TabIndex = 0;
            this.SignatureRTBox.Text = "";
            // 
            // FileInfoWrapperTLPanel
            // 
            this.FileInfoWrapperTLPanel.ColumnCount = 1;
            this.FileInfoWrapperTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FileInfoWrapperTLPanel.Controls.Add(this.FileInfoGBox, 0, 0);
            this.FileInfoWrapperTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileInfoWrapperTLPanel.Location = new System.Drawing.Point(10, 242);
            this.FileInfoWrapperTLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.FileInfoWrapperTLPanel.Name = "FileInfoWrapperTLPanel";
            this.FileInfoWrapperTLPanel.RowCount = 1;
            this.FileInfoWrapperTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FileInfoWrapperTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 236F));
            this.FileInfoWrapperTLPanel.Size = new System.Drawing.Size(455, 236);
            this.FileInfoWrapperTLPanel.TabIndex = 1;
            // 
            // FileInfoGBox
            // 
            this.FileInfoGBox.Controls.Add(this.FileInfoTLPanel);
            this.FileInfoGBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileInfoGBox.Location = new System.Drawing.Point(3, 3);
            this.FileInfoGBox.Name = "FileInfoGBox";
            this.FileInfoGBox.Padding = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.FileInfoGBox.Size = new System.Drawing.Size(449, 230);
            this.FileInfoGBox.TabIndex = 0;
            this.FileInfoGBox.TabStop = false;
            this.FileInfoGBox.Text = "Súborové informácie";
            // 
            // FileInfoTLPanel
            // 
            this.FileInfoTLPanel.ColumnCount = 2;
            this.FileInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.FileInfoTLPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.FileInfoTLPanel.Controls.Add(this.AbsPathValueLabel, 1, 2);
            this.FileInfoTLPanel.Controls.Add(this.FileTypeLabel, 0, 3);
            this.FileInfoTLPanel.Controls.Add(this.FileNameLabel, 0, 0);
            this.FileInfoTLPanel.Controls.Add(this.AbsPathLabel, 0, 2);
            this.FileInfoTLPanel.Controls.Add(this.CreationDateLabel, 0, 1);
            this.FileInfoTLPanel.Controls.Add(this.CreationDateValueLabel, 1, 1);
            this.FileInfoTLPanel.Controls.Add(this.FileNameValueLabel, 1, 0);
            this.FileInfoTLPanel.Controls.Add(this.FileTypeValueLabel, 1, 3);
            this.FileInfoTLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileInfoTLPanel.Location = new System.Drawing.Point(3, 26);
            this.FileInfoTLPanel.Margin = new System.Windows.Forms.Padding(15);
            this.FileInfoTLPanel.MaximumSize = new System.Drawing.Size(0, 200);
            this.FileInfoTLPanel.Name = "FileInfoTLPanel";
            this.FileInfoTLPanel.Padding = new System.Windows.Forms.Padding(5);
            this.FileInfoTLPanel.RowCount = 4;
            this.FileInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.FileInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.FileInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.FileInfoTLPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.FileInfoTLPanel.Size = new System.Drawing.Size(443, 200);
            this.FileInfoTLPanel.TabIndex = 6;
            // 
            // AbsPathValueLabel
            // 
            this.AbsPathValueLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.AbsPathValueLabel.AutoSize = true;
            this.AbsPathValueLabel.Location = new System.Drawing.Point(285, 112);
            this.AbsPathValueLabel.Name = "AbsPathValueLabel";
            this.AbsPathValueLabel.Size = new System.Drawing.Size(46, 20);
            this.AbsPathValueLabel.TabIndex = 12;
            this.AbsPathValueLabel.Text = "value";
            // 
            // FileTypeLabel
            // 
            this.FileTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.FileTypeLabel.AutoSize = true;
            this.FileTypeLabel.Location = new System.Drawing.Point(5, 161);
            this.FileTypeLabel.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.FileTypeLabel.Name = "FileTypeLabel";
            this.FileTypeLabel.Size = new System.Drawing.Size(38, 20);
            this.FileTypeLabel.TabIndex = 7;
            this.FileTypeLabel.Text = "Typ:";
            this.FileTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FileNameLabel
            // 
            this.FileNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.FileNameLabel.AutoSize = true;
            this.FileNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileNameLabel.Location = new System.Drawing.Point(5, 19);
            this.FileNameLabel.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.FileNameLabel.Name = "FileNameLabel";
            this.FileNameLabel.Size = new System.Drawing.Size(57, 20);
            this.FileNameLabel.TabIndex = 4;
            this.FileNameLabel.Text = "Názov:";
            this.FileNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AbsPathLabel
            // 
            this.AbsPathLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.AbsPathLabel.AutoSize = true;
            this.AbsPathLabel.Location = new System.Drawing.Point(5, 113);
            this.AbsPathLabel.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.AbsPathLabel.Name = "AbsPathLabel";
            this.AbsPathLabel.Size = new System.Drawing.Size(128, 20);
            this.AbsPathLabel.TabIndex = 5;
            this.AbsPathLabel.Text = "Absolútna cesta:";
            this.AbsPathLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CreationDateLabel
            // 
            this.CreationDateLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.CreationDateLabel.AutoSize = true;
            this.CreationDateLabel.Location = new System.Drawing.Point(5, 66);
            this.CreationDateLabel.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.CreationDateLabel.Name = "CreationDateLabel";
            this.CreationDateLabel.Size = new System.Drawing.Size(135, 20);
            this.CreationDateLabel.TabIndex = 6;
            this.CreationDateLabel.Text = "Dátum vytvorenia:";
            this.CreationDateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CreationDateValueLabel
            // 
            this.CreationDateValueLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CreationDateValueLabel.AutoSize = true;
            this.CreationDateValueLabel.Location = new System.Drawing.Point(285, 65);
            this.CreationDateValueLabel.Name = "CreationDateValueLabel";
            this.CreationDateValueLabel.Size = new System.Drawing.Size(46, 20);
            this.CreationDateValueLabel.TabIndex = 9;
            this.CreationDateValueLabel.Text = "value";
            // 
            // FileNameValueLabel
            // 
            this.FileNameValueLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FileNameValueLabel.AutoSize = true;
            this.FileNameValueLabel.Location = new System.Drawing.Point(285, 18);
            this.FileNameValueLabel.Name = "FileNameValueLabel";
            this.FileNameValueLabel.Size = new System.Drawing.Size(46, 20);
            this.FileNameValueLabel.TabIndex = 10;
            this.FileNameValueLabel.Text = "value";
            this.FileNameValueLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FileTypeValueLabel
            // 
            this.FileTypeValueLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FileTypeValueLabel.AutoSize = true;
            this.FileTypeValueLabel.Location = new System.Drawing.Point(285, 160);
            this.FileTypeValueLabel.Name = "FileTypeValueLabel";
            this.FileTypeValueLabel.Size = new System.Drawing.Size(46, 20);
            this.FileTypeValueLabel.TabIndex = 11;
            this.FileTypeValueLabel.Text = "value";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.MessageManipulateFLPanel, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 528);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(949, 75);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.UnzipButton);
            this.flowLayoutPanel1.Controls.Add(this.ZipButton);
            this.flowLayoutPanel1.Controls.Add(this.VerifySignatureButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(474, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(475, 55);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // UnzipButton
            // 
            this.UnzipButton.Location = new System.Drawing.Point(335, 10);
            this.UnzipButton.Margin = new System.Windows.Forms.Padding(3, 10, 15, 3);
            this.UnzipButton.Name = "UnzipButton";
            this.UnzipButton.Size = new System.Drawing.Size(125, 40);
            this.UnzipButton.TabIndex = 1;
            this.UnzipButton.Text = "Odzipuj";
            this.UnzipButton.UseVisualStyleBackColor = true;
            this.UnzipButton.Click += new System.EventHandler(this.UnzipButton_Click);
            // 
            // ZipButton
            // 
            this.ZipButton.Location = new System.Drawing.Point(197, 10);
            this.ZipButton.Margin = new System.Windows.Forms.Padding(3, 10, 10, 3);
            this.ZipButton.Name = "ZipButton";
            this.ZipButton.Size = new System.Drawing.Size(125, 40);
            this.ZipButton.TabIndex = 1;
            this.ZipButton.Text = "Zazipuj";
            this.ZipButton.UseVisualStyleBackColor = true;
            this.ZipButton.Click += new System.EventHandler(this.ZipButton_Click);
            // 
            // VerifySignatureButton
            // 
            this.VerifySignatureButton.Location = new System.Drawing.Point(59, 10);
            this.VerifySignatureButton.Margin = new System.Windows.Forms.Padding(3, 10, 10, 3);
            this.VerifySignatureButton.Name = "VerifySignatureButton";
            this.VerifySignatureButton.Size = new System.Drawing.Size(125, 40);
            this.VerifySignatureButton.TabIndex = 2;
            this.VerifySignatureButton.Text = "Over podpis";
            this.VerifySignatureButton.UseVisualStyleBackColor = true;
            this.VerifySignatureButton.Click += new System.EventHandler(this.VerifySignatureButton_Click);
            // 
            // MessageManipulateFLPanel
            // 
            this.MessageManipulateFLPanel.Controls.Add(this.SaveMessageButton);
            this.MessageManipulateFLPanel.Controls.Add(this.InsertFileButton);
            this.MessageManipulateFLPanel.Controls.Add(this.SaveSignatureButton);
            this.MessageManipulateFLPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageManipulateFLPanel.Location = new System.Drawing.Point(0, 0);
            this.MessageManipulateFLPanel.Margin = new System.Windows.Forms.Padding(0);
            this.MessageManipulateFLPanel.Name = "MessageManipulateFLPanel";
            this.MessageManipulateFLPanel.Size = new System.Drawing.Size(474, 55);
            this.MessageManipulateFLPanel.TabIndex = 3;
            // 
            // SaveMessageButton
            // 
            this.SaveMessageButton.Location = new System.Drawing.Point(15, 10);
            this.SaveMessageButton.Margin = new System.Windows.Forms.Padding(15, 10, 10, 3);
            this.SaveMessageButton.Name = "SaveMessageButton";
            this.SaveMessageButton.Size = new System.Drawing.Size(125, 40);
            this.SaveMessageButton.TabIndex = 4;
            this.SaveMessageButton.Text = "Exportuj";
            this.SaveMessageButton.UseVisualStyleBackColor = true;
            this.SaveMessageButton.Click += new System.EventHandler(this.SaveMessageButton_Click_1);
            // 
            // InsertFileButton
            // 
            this.InsertFileButton.Location = new System.Drawing.Point(153, 10);
            this.InsertFileButton.Margin = new System.Windows.Forms.Padding(3, 10, 10, 3);
            this.InsertFileButton.Name = "InsertFileButton";
            this.InsertFileButton.Size = new System.Drawing.Size(125, 40);
            this.InsertFileButton.TabIndex = 5;
            this.InsertFileButton.Text = "Vlož súbor";
            this.InsertFileButton.UseVisualStyleBackColor = true;
            this.InsertFileButton.Click += new System.EventHandler(this.InsertFileButton_Click);
            // 
            // SaveSignatureButton
            // 
            this.SaveSignatureButton.Location = new System.Drawing.Point(291, 10);
            this.SaveSignatureButton.Margin = new System.Windows.Forms.Padding(3, 10, 10, 3);
            this.SaveSignatureButton.Name = "SaveSignatureButton";
            this.SaveSignatureButton.Size = new System.Drawing.Size(125, 40);
            this.SaveSignatureButton.TabIndex = 3;
            this.SaveSignatureButton.Text = "Podpíš";
            this.SaveSignatureButton.UseVisualStyleBackColor = true;
            this.SaveSignatureButton.Click += new System.EventHandler(this.SaveSignatureButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 656);
            this.Controls.Add(this.MainTabControl);
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RSA šifra";
            this.MainTabControl.ResumeLayout(false);
            this.KeysTabPage.ResumeLayout(false);
            this.KeyTLPanel.ResumeLayout(false);
            this.KeyManipulateTLPanel.ResumeLayout(false);
            this.KeySettingsFLayout.ResumeLayout(false);
            this.KeySettingsFLayout.PerformLayout();
            this.keyManTLPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.PrivateKeyGBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.PublicKeyGBox.ResumeLayout(false);
            this.PublicKeyTLPanel.ResumeLayout(false);
            this.PublicKeyTLPanel.PerformLayout();
            this.EncryptionTabPage.ResumeLayout(false);
            this.EncryptionTLPanel.ResumeLayout(false);
            this.EncryptIOTLPanel.ResumeLayout(false);
            this.EncryptIOTLPanel.PerformLayout();
            this.EncryptManipulateTLPanel.ResumeLayout(false);
            this.EncryptFLPanel.ResumeLayout(false);
            this.LeftEncFLPanel.ResumeLayout(false);
            this.DecryptionTabPage.ResumeLayout(false);
            this.DecryptionTLPanel.ResumeLayout(false);
            this.DecryptManipulateTLPanel.ResumeLayout(false);
            this.DecryptFLPanel.ResumeLayout(false);
            this.DecryptIOTLPanel.ResumeLayout(false);
            this.DecryptIOTLPanel.PerformLayout();
            this.SignatureTabPage.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.FileInfoWrapperTLPanel.ResumeLayout(false);
            this.FileInfoGBox.ResumeLayout(false);
            this.FileInfoTLPanel.ResumeLayout(false);
            this.FileInfoTLPanel.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.MessageManipulateFLPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MainTabControl;
        private System.Windows.Forms.TabPage KeysTabPage;
        private System.Windows.Forms.TabPage EncryptionTabPage;
        private System.Windows.Forms.TabPage DecryptionTabPage;
        private System.Windows.Forms.TableLayoutPanel KeyTLPanel;
        private System.Windows.Forms.TableLayoutPanel EncryptionTLPanel;
        private System.Windows.Forms.TableLayoutPanel DecryptionTLPanel;
        private System.Windows.Forms.TableLayoutPanel EncryptIOTLPanel;
        private System.Windows.Forms.RichTextBox OriginalTextRTBox;
        private System.Windows.Forms.RichTextBox EncryptedTextRTBox;
        private System.Windows.Forms.Label OriginalTextLabel;
        private System.Windows.Forms.Label EncryptedTextLabel;
        private System.Windows.Forms.TableLayoutPanel DecryptIOTLPanel;
        private System.Windows.Forms.RichTextBox TextToDecryptRTBox;
        private System.Windows.Forms.RichTextBox DecryptedTextRTBox;
        private System.Windows.Forms.Label TextToDecryptLabel;
        private System.Windows.Forms.Label DecryptedTextLabel;
        private System.Windows.Forms.TableLayoutPanel EncryptManipulateTLPanel;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.TableLayoutPanel DecryptManipulateTLPanel;
        private System.Windows.Forms.Button DecryptButton;
        private System.Windows.Forms.Button InsertButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox PrivateKeyGBox;
        private System.Windows.Forms.GroupBox PublicKeyGBox;
        private System.Windows.Forms.TableLayoutPanel PublicKeyTLPanel;
        private System.Windows.Forms.Label PublicModulusLabel;
        private System.Windows.Forms.RichTextBox PublicModulRTBox;
        private System.Windows.Forms.RichTextBox PublicExponentRTBox;
        private System.Windows.Forms.Label PublicExponentLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RichTextBox PrivateExponentRTBox;
        private System.Windows.Forms.Label PrivateExponentLabel;
        private System.Windows.Forms.Label PrivateModulusLabel;
        private System.Windows.Forms.RichTextBox PrivateModulRTBox;
        private System.Windows.Forms.TableLayoutPanel KeyManipulateTLPanel;
        private System.Windows.Forms.Button GenerateKeysButton;
        private System.Windows.Forms.FlowLayoutPanel KeySettingsFLayout;
        private System.Windows.Forms.ComboBox ExponentCBox;
        private System.Windows.Forms.Label BitLengthLabel;
        private System.Windows.Forms.ComboBox BitLengthCBox;
        private System.Windows.Forms.Label ExponentLabel;
        private System.Windows.Forms.FlowLayoutPanel DecryptFLPanel;
        private System.Windows.Forms.Button ClearDecryptTabButton;
        private System.Windows.Forms.FlowLayoutPanel EncryptFLPanel;
        private System.Windows.Forms.Button EncryptButton;
        private System.Windows.Forms.Button ClearEncryptTabButton;
        private System.Windows.Forms.TableLayoutPanel keyManTLPanel;
        private System.Windows.Forms.Button ExportKeysButton;
        private System.Windows.Forms.FlowLayoutPanel LeftEncFLPanel;
        private System.Windows.Forms.TabPage SignatureTabPage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.RichTextBox MessageRTBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label FileOverViewLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button UnzipButton;
        private System.Windows.Forms.Button ZipButton;
        private System.Windows.Forms.FlowLayoutPanel MessageManipulateFLPanel;
        private System.Windows.Forms.Button SaveSignatureButton;
        private System.Windows.Forms.Button SaveMessageButton;
        private System.Windows.Forms.Button InsertFileButton;
        private System.Windows.Forms.Button VerifySignatureButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.RichTextBox SignatureRTBox;
        private System.Windows.Forms.TableLayoutPanel FileInfoWrapperTLPanel;
        private System.Windows.Forms.GroupBox FileInfoGBox;
        private System.Windows.Forms.TableLayoutPanel FileInfoTLPanel;
        private System.Windows.Forms.Label AbsPathValueLabel;
        private System.Windows.Forms.Label FileTypeLabel;
        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.Label AbsPathLabel;
        private System.Windows.Forms.Label CreationDateLabel;
        private System.Windows.Forms.Label CreationDateValueLabel;
        private System.Windows.Forms.Label FileNameValueLabel;
        private System.Windows.Forms.Label FileTypeValueLabel;
        private System.Windows.Forms.Button InsertKeysButton;
    }
}

